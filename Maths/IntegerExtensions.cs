﻿using System.Collections.Generic;

namespace Maths
{
	public static class IntegerExtensions
	{
		private const double Tolerance = 0.00000;

		public static int AbsoluteValue(this int value)
		{
			var result = (value >= 0) ? value : (value * -1);

			return result;
		}

		public static double SquareRoot(this int radicand)
		{
			var tooHigh = radicand / 2.0;
			var tooLow = radicand / tooHigh;
			var delta = tooHigh - tooLow;
			var iterations = 0;

			while ((delta.AbsoluteValue() > Tolerance) && iterations++ < 100)
			{
				tooHigh = (tooHigh + tooLow) / 2.0;
				tooLow = radicand / tooHigh;
				delta = tooHigh - tooLow;
			}

			return tooLow;
		}

		public static IEnumerable<int> Factors(this int factorBase)
		{
			var factors = new List<int>();

			var possible = 2;

			while (possible <= factorBase.SquareRoot())
			{
				if((factorBase % possible) == 0)
				{
					var pair = factorBase / possible;

					factors.Add(possible);

					if (pair != possible)
					{
						factors.Add(pair);
					}
				}

				possible++;
			}

			factors.Sort();

			return factors;
		}
	}
}
