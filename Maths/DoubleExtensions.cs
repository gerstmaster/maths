﻿namespace Maths
{
	public static class DoubleExtensions
	{
		public static double AbsoluteValue(this double value)
		{
			var result = (value >= 0) ? value : (value * -1);

			return result;
		}
	}
}
