﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Maths.Tests
{
	[TestFixture]
	public class IntegerExtensionsTests
	{
		[Test]
		public void SquareRootOfOneIsOne()
		{
			var root = 1.SquareRoot();

			Assert.That(root, Is.EqualTo(1));
		}

		[Test]
		public void SquareRootOfTwoIsCorrect()
		{
			var root = 2.SquareRoot();

			Assert.That(root, Is.EqualTo(Math.Sqrt(2)));
		}

		[Test]
		public void SquareRootOfFourIsTwo()
		{
			var root = 4.SquareRoot();

			Assert.That(root, Is.EqualTo(2));
		}

		[Test]
		public void SquareRootOfNineIsThree()
		{
			var root = 9.SquareRoot();

			Assert.That(root, Is.EqualTo(3));
		}

		[Test]
		public void FactorOfOneIsEmpty()
		{
			var factors = 1.Factors();

			Assert.That(factors, Is.Empty);
		}

		[Test]
		public void FactorsOfTwoIsEmpty()
		{
			var factors = 2.Factors();

			Assert.That(factors, Is.Empty);
		}

		[Test]
		public void FactorsOfThreeIsEmpty()
		{
			var factors = 3.Factors();

			Assert.That(factors, Is.Empty);
		}

		[Test]
		public void FactorsOfFourHasOneItem()
		{
			var factors = 4.Factors();

			Assert.That(factors, Has.Count.EqualTo(1));
		}

		[Test]
		public void FactorsOfFourIsTwo()
		{
			var factors = 4.Factors();

			Assert.That(factors.FirstOrDefault(), Is.EqualTo(2));
		}

		[Test]
		public void FactorsOfTwelveIsComplete()
		{
			var factors = 12.Factors().ToList();

			Assert.That(factors, Has.Count.EqualTo(4));

			Assert.That(factors[0], Is.EqualTo(2));
			Assert.That(factors[1], Is.EqualTo(3));
			Assert.That(factors[2], Is.EqualTo(4));
			Assert.That(factors[3], Is.EqualTo(6));
		}

		[Test]
		public void AbsoluteValueOfZeroIsZero()
		{
			const int zero = 0;
			var absValue = zero.AbsoluteValue();

			Assert.That(absValue, Is.EqualTo(0));
		}

		[Test]
		public void AbsoluteValueOfNegativeNumberIsPositiveNumber()
		{
			const int negative = -5;
			var absValue = negative.AbsoluteValue();

			Assert.That(absValue, Is.EqualTo(5));
		}

		[Test]
		public void AbsoluteValueOfPositiveNumberIsPositiveNumber()
		{
			const int positive = 5;
			var absValue = positive.AbsoluteValue();

			Assert.That(absValue, Is.EqualTo(5));
		}
	}
}
