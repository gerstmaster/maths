﻿using NUnit.Framework;

namespace Maths.Tests
{
	[TestFixture]
	public class DoubleExtensionsTests
	{
		[Test]
		public void AbsoluteValueOfZeroIsZero()
		{
			const double zero = 0;

			var absValue = zero.AbsoluteValue();

			Assert.That(absValue, Is.EqualTo(0));
		}

		[Test]
		public void AbsoluteValueOfNegativeNumberIsPositiveNumber()
		{
			const double negative = -5.5;

			var absValue = negative.AbsoluteValue();

			Assert.That(absValue, Is.EqualTo(5.5));
		}

		[Test]
		public void AbsoluteValueOfPositiveNumberIsPositiveNumber()
		{
			const double positive = 5.5;

			var absValue = positive.AbsoluteValue();

			Assert.That(absValue, Is.EqualTo(5.5));
		}
	}
}
